# frozen_string_literal: true

class Util
  def self.dry_run?
    if ENV['DRY_RUN'].nil?
      true
    else
      ENV['DRY_RUN'] == 'true'
    end
  end
end
