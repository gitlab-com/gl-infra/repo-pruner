# frozen_string_literal: true

require_relative 'util'

require 'active_support/core_ext/integer'
require 'httparty'
require 'json'
require 'ruby-progressbar'
require 'time'

class Prune
  def initialize(repo_name, age_days, max_count_to_remove)
    age_days ||= 30
    @comparison_time = age_days.to_i.days.ago
    puts "Looking for items older than #{@comparison_time}"

    @dry_run = Util.dry_run?
    @repo_name = repo_name
    @token = ENV.fetch('PACKAGECLOUD_API_TOKEN') { |name| raise "Missing environment variable `#{name}`" }
    @base_url = "https://#{@token}:@packages.gitlab.com"
    if max_count_to_remove.nil?
      @max_count_to_remove = Float::INFINITY
    else
      @max_count_to_remove = max_count_to_remove.to_i
    end
  end

  def execute
    page_number = 1
    response = HTTParty.get(packages_url(page_number))
    raise "Problem making GET to packagecloud: #{response}" unless response.success?

    total_packages = response.headers['total'].to_i
    pg = ProgressBar.create(
      total: total_packages,
      format: "%e %c of %C: %b\u{15E7}%i %p%% %t",
      progress_mark: ' ',
      remainder_mark: "\u{FF65}"
    )

    removed_packages = 0
    enumermerated_packages = 0

    while next?(response.headers['link']) || (removed_packages < @max_count_to_remove && enumermerated_packages < total_packages)
      response = HTTParty.get(packages_url(page_number))

      sorted_json_body = response.sort_by { |x| Time.parse(x['created_at']) }
      enumermerated_packages += sorted_json_body.count

      candidate_packages = sorted_json_body.find_all do |x|
        Time.parse(x['created_at']) <= @comparison_time
      end

      candidate_packages.each do |i|
        if removed_packages < @max_count_to_remove
          puts "would purge: #{i['filename']}: #{i['created_at']} via #{i['destroy_url']}" if @dry_run
          removed_packages += 1 if destroy("#{@base_url}#{i['destroy_url']}")
        end
      end
      page_number += 1 if candidate_packages.count.zero?
      pg.progress += response.count.to_i
    end
    pg.finish

    print_summary(removed_packages, total_packages)
  end

  private

  def packages_url(page_number)
    "#{@base_url}/api/v1/repos/gitlab/#{@repo_name}/packages.json?page=#{page_number}"
  end

  def destroy(url)
    return true if @dry_run

    HTTParty.delete(url).success?
  end

  def next?(header_link)
    header_link&.include? 'next'
  end

  def print_summary(removed_packages, total_packages)
    puts "Total Packages: #{total_packages}"
    if @dry_run
      puts "Would remove #{removed_packages} packages"
    else
      puts "Removed Packages: #{removed_packages}"
    end
  end
end
