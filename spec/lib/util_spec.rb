require 'spec_helper'

describe Util do
  describe 'dry_run defined' do
    it 'true returns true' do
      allow(ENV).to receive(:[]).with('DRY_RUN').and_return('true')
      expect(described_class.dry_run?).to be true
    end

    it 'to something random returns false' do
      allow(ENV).to receive(:[]).with('DRY_RUN').and_return('foobar')
      expect(described_class.dry_run?).to be false
    end

    it 'false returns false' do
      allow(ENV).to receive(:[]).with('DRY_RUN').and_return('false')
      expect(described_class.dry_run?).to be false
    end
  end

  describe 'dry_run undefined' do
    it 'returns true' do
      expect(described_class.dry_run?).to be true
    end
  end
end
