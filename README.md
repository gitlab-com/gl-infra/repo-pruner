## repo-pruner

Removes packages from a packagecloud repository that are older than 30 days (by
default, see [#arguments](#arguments)).

## `prune[repo_name, age_days, max_count_to_remove]`

This task will:

1. Query for all packages in the specified repository
1. Remove all packages that are older than `age_days` AND/OR
1. Remove a maximum number of packages as defined by `max_count_to_remove`

By default, this task will operate in a dry run mode.  Set the env var to
prevent such from happening.

### Arguments

| Argument              | Required | Default  | Description |
| --------              | -------- | -------  | ----------- |
| `repo_name`           | yes      |          | Specifies the name of the repo this tool will search |
| `age_days`            | no       | 30       | Specifies the age in days for which we start removing packages |
| `max_count_to_remove` | no       | infinity | Sets the max number of packages that we should remove |

### Configuration

| Variable                 | Required | Purpose |
| --------                 | -------- | ------- |
| `DRY_RUN`                | no       | Verbosely output what WOULD be done |
| `PACKAGECLOUD_API_TOKEN` | yes      | Provides access to our packagecloud instance |

### Examples

```sh
% bundle exec rake 'package:prune[nightly-builds, 30, 1]'
would purge: gitlab-ce_7.14.0+20150826000010.git.79.6a40008-ce.0_amd64.deb: 2015-08-26T00:28:33.000Z via /api/v1/repos/gitlab/nightly-builds/ubuntu/trusty/gitlab-ce_7.14.0+20150826000010.git.79.6a40008-ce.0_amd64.deb････････････････ 0% Pro
gress
Time: 00:00:00 26337 of 26337:                                   ᗧ 100% Progress
Total Packages: 26337
Would remove Packages 1

% DRY_RUN=false bundle exec rake 'package:prune[nightly-builds, 30, 1]'
Time: 00:00:01 26337 of 26337:                                   ᗧ 100% Progress
Total Packages: 26337
Removed Packages 1
```
